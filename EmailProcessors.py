import logging
import email
import boto3
from sentrycommon import Configuration
import bs4
import re
import requests
import base64, quopri

""" 
Classes to handle creating JSONS out of different camera emails
"""


def load_email(key, trigger_bucket):
    logger = logging.getLogger(__name__)
    s3_boto_client = boto3.client('s3', region_name='us-west-2',
                                  aws_access_key_id=Configuration.AWS_ACCESS_KEY,
                                  aws_secret_access_key=Configuration.AWS_SECRET_KEY)
    logger.info("Bucket: {0} Key: {1}".format(trigger_bucket, key))

    file = s3_boto_client.get_object(Bucket=trigger_bucket, Key=key)
    body = file['Body'].read()
    msg = email.message_from_bytes(body)
    return msg


def get_right_processor(msg):
    logger = logging.getLogger(__name__)
    subject = msg['subject']
    logger.info("Email Subject: {0}".format(subject))
    sent_from = msg['from']
    logger.info("Email sent_from: {0}".format(sent_from))

    if "arlo" in sent_from:
        return ArloProcessor()
    elif "IPCamera" in sent_from:
        return FoscamProcessor()
    elif "Your" and "camera" in subject:
        return NestCamProcessor()
    elif "?UTF-8?B" in subject:
        return AmcrestProcessor()

    return DlinkProcessor()


class DlinkProcessor():
    def __init__(self):
        self.logger = logging.getLogger(__name__)

    def get_camera_id(self, msg):
        subject = msg['subject']
        camera_id = subject.split(" ")[3]
        return camera_id

    def get_image_bytes(self, msg):
        msg.get_payload()
        attachment = msg.get_payload()[1]
        image_bytes = attachment.get_payload(decode=True)
        return image_bytes


class ArloProcessor():
    def __init__(self):
        self.logger = logging.getLogger(__name__)

    def get_camera_id(self, msg):
        delimeter = "#$%^"
        html_text = str(msg.get_payload()[0])
        soup = bs4.BeautifulSoup(html_text, "html.parser")
        message_body_div = soup.body.find_all('div')[1]
        letter_div = message_body_div.find_all('div')[2]
        interested_text = letter_div.find_all('p')[2].contents[0]
        interested_text = interested_text.replace('\n', ' ')
        camera_id = interested_text
        if delimeter in interested_text:
            print (interested_text)
            camera_id = interested_text.split(delimeter)[1]
            if '=' in camera_id:
                camera_id = camera_id.replace(" ", "")
                camera_id = camera_id.split('=')
                camera_id = camera_id[0] + camera_id[1]
        self.logger.info('Arlo Camera ID: {0}'.format(camera_id))
        return camera_id

    def get_image_bytes(self, msg):
        r = requests.get(self.__get_arlo_S3_url(msg))
        image_raw = r.content
        return image_raw

    def __get_arlo_S3_url(self, msg):
        got_it = re.search(r"src=3D\"", str(msg))
        st = {"\""}
        new_str = str(msg)
        new_str = new_str[got_it.span()[1]:len(new_str)]
        ind = next((i for i, ch in enumerate(new_str) if ch in st), None)
        new_str = new_str[0:ind]
        new_str = new_str.replace("=3D", "=")
        img_url = new_str.replace("=\n", "")
        self.logger.debug(('Arlo S3 URL is -----------> %s') % img_url)
        return img_url


class FoscamProcessor():
    def __init__(self):
        self.logger = logging.getLogger(__name__)

    # def load_email(self, key, trigger_bucket):
    #     s3_boto_client = boto3.client('s3', region_name='us-west-2',
    #                                   aws_access_key_id=Configuration.AWS_ACCESS_KEY,
    #                                   aws_secret_access_key=Configuration.AWS_SECRET_KEY)
    #     self.logger.info("Bucket: {0} Key: {1}".format(trigger_bucket, key))
    #     file = s3_boto_client.get_object(Bucket=trigger_bucket, Key=key)
    #     body = file['Body'].read()
    #     msg = email.message_from_bytes(body)
    #     return msg


    def get_camera_id(self, msg):
        sent_from = msg['from']
        camera_id = sent_from.split("@")[0]
        camera_id = camera_id.split("<")[1]
        html_text = str(msg.get_payload()[0])
        for item in html_text.split('\n'):
            if "Your IPCamera:" in item:
                id_parser = item.split(':')
                id_array = id_parser[1].split()
                camera_id = id_array[0]
        return camera_id

    def get_image_bytes(self, msg):
        msg.get_payload()
        attachment = msg.get_payload()[1]
        image_bytes = attachment.get_payload(decode=True)
        return image_bytes

class NestCamProcessor():
    def __init__(self):
        self.logger = logging.getLogger(__name__)

    # def load_email(self, key, trigger_bucket):
    #     s3_boto_client = boto3.client('s3', region_name='us-west-2',
    #                                   aws_access_key_id=Configuration.AWS_ACCESS_KEY,
    #                                   aws_secret_access_key=Configuration.AWS_SECRET_KEY)
    #     self.logger.info("Bucket: {0} Key: {1}".format(trigger_bucket, key))
    #     file = s3_boto_client.get_object(Bucket=trigger_bucket, Key=key)
    #     body = file['Body'].read()
    #     msg = email.message_from_bytes(body)
    #     return msg

    def get_camera_id(self, msg):
        subject = msg['subject']
        id_parser = re.findall(r'Your (.*?) camera',subject)
        camera_id = id_parser[0]
        return camera_id

    def get_image_bytes(self, msg):
        msg.get_payload()
        attachment = msg.get_payload()[1]
        image_bytes = attachment.get_payload(decode=True)
        return image_bytes


class AmcrestProcessor():
    def __init__(self):
        self.logger = logging.getLogger(__name__)

    # def load_email(self, key, trigger_bucket):
    #     s3_boto_client = boto3.client('s3', region_name='us-west-2',
    #                                   aws_access_key_id=Configuration.AWS_ACCESS_KEY,
    #                                   aws_secret_access_key=Configuration.AWS_SECRET_KEY)
    #     self.logger.info("Bucket: {0} Key: {1}".format(trigger_bucket, key))
    #     file = s3_boto_client.get_object(Bucket=trigger_bucket, Key=key)
    #     body = file['Body'].read()
    #     msg = email.message_from_bytes(body)
    #     return msg

    def get_camera_id(self, msg):
        subject = msg['subject']
        subject = self.__encoded_words_to_text(subject)
        camera_id = subject
        return camera_id

    def __encoded_words_to_text(self, encoded_words):
        encoded_word_regex = r'=\?{1}(.+)\?{1}([B|Q])\?{1}(.+)\?{1}='
        charset, encoding, encoded_text = re.match(encoded_word_regex, encoded_words).groups()
        if encoding is 'B':
            byte_string = base64.b64decode(encoded_text)
        elif encoding is 'Q':
            byte_string = quopri.decodestring(encoded_text)
        return byte_string.decode(charset)

    def get_image_bytes(self, msg):
        msg.get_payload()
        attachment = msg.get_payload()[1]
        image_bytes = attachment.get_payload(decode=True)
        return image_bytes
