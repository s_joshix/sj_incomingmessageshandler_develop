from sentrycommon import Logger
import logging
import EmailProcessors
import datetime
import json
import calendar
from sentrycommon import SentryAWSClients, Configuration as conf

log_level = Logger.Logger_Setup()
logger = logging.getLogger(__name__)
logger.info("Environment Variables Set: {0}:{1}, {2}:{3}: {4}:{5}"
             .format('SQS_Queue', conf.SQS_QUEUE_NAME,
                     'RESULT_LAMBDA', conf.PEOPLE_RESULT_HANDLER_LAMBDA_NAME,
                     'call_twilio', conf.TWILIO_LAMBDA_NAME))

log_level.set_level(logger)


def get_data(event):
    email_key = event['Records'][0]['s3']['object']['key']
    email_bucket = event['Records'][0]['s3']['bucket']['name']

    extracted_info = {}
    current_time = calendar.timegm(datetime.datetime.utcnow().timetuple())
    extracted_info[conf.REC_KEY_CODE] = email_key + ".jpg"

    msg = EmailProcessors.load_email(email_key, email_bucket)
    email_processor = EmailProcessors.get_right_processor(msg)

    extracted_info[conf.IMAGE_CODE] ="{0}@{1}".format(email_processor.get_camera_id(msg), current_time)
    image_bytes = email_processor.get_image_bytes(msg)

    logger.info("Obtained Image ID: {0}".format(extracted_info[conf.IMAGE_CODE]))

    return extracted_info, image_bytes


def store_in_s3(extracted_info, image_bytes):
    key = extracted_info[conf.REC_KEY_CODE]
    bucket = conf.IMAGES_BUCKET_NAME
    s3_client = SentryAWSClients.SentryS3()
    s3_client.put_into_s3(bucket, key, image_bytes)
    logger.info("Storing image_bytes into S3 key: {0}, bucket:{1}".format(key, bucket))


def send_event_to_sqs(extracted_info):
    queue = SentryAWSClients.SentrySQSQueue(queue_name=conf.SQS_QUEUE_NAME)
    needed_info = {
        conf.IMAGE_CODE: extracted_info[conf.IMAGE_CODE],
    }
    queue.send_message(json.dumps(needed_info))


def store_info_into_database(extracted_info):
    dynamo_client = SentryAWSClients.SentryDynamo()
    image_code = extracted_info[conf.IMAGE_CODE]
    info_to_store = {key:value for (key, value) in extracted_info.items() if key != conf.IMAGE_CODE}
    logger.info("Storing into database: Key: {0}, Info: {1}".format(image_code, extracted_info))
    dynamo_client.set_image_properties(image_code, info_to_store)


def lambda_handler(event, _context):
    logger.debug("Environment Variables Set\n {0}:{1}\n {2}:{3}\n {4}:{5}"
                .format('SQS_Queue', conf.SQS_QUEUE_NAME,
                        'RESULT_LAMBDA', conf.PEOPLE_RESULT_HANDLER_LAMBDA_NAME,
                        'call_twilio', conf.TWILIO_LAMBDA_NAME))

    extracted_info, image_bytes = get_data(event)
    store_in_s3(extracted_info, image_bytes)
    store_info_into_database(extracted_info)
    send_event_to_sqs(extracted_info)


