from sentrycommon import SentryAWSClients, Logger

import os
import logging
import logging.handlers
import calendar
import datetime

from EmailProcessors import *


log_level = Logger.Logger_Setup('DEBUG')
logger = logging.getLogger(__name__)
log_level.set_level(logger)

dynamo_client = SentryAWSClients.SentryDynamo()
camera_id="S-CWHLP9"
start_time=1533233040

def test_pi_camera ():
    logging.error('testing Incoming images from Pi')
    last_img = get_latest_image(camera_id, start_time)
    logging.error('last image found at ' + str(last_img))
    assert last_img is not None

def test_dlink_2330_camera ():
    logging.error('testing Incoming images from DLink-2330')
    last_img = get_latest_image(camera_id, start_time)
    logging.error('last image found at ' + str(last_img))
    assert last_img is not None

#@staticmethod
def get_latest_image (camera_id, start_time):
    current_time = calendar.timegm(datetime.datetime.utcnow().timetuple())
    maximum_time = current_time
    dynamo_client = SentryAWSClients.SentryDynamo()
    #s3_client = SentryAWSClients.SentryS3()
    last_n_hours_images = dynamo_client.get_images_between_time(camera_id, minimum_time=start_time, maximum_time=maximum_time)
    if len(last_n_hours_images) < 1:
        return None
    logging.info('last image length is ' + str(len(last_n_hours_images)))
    last_image = last_n_hours_images[-1]
    return last_image
