import os
import logging

import EmailProcessors as ep
from sentrycommon import SentryAWSClients, Logger


log_level = Logger.Logger_Setup('DEBUG')
logger = logging.getLogger(__name__)
log_level.set_level(logger)

s3_client = SentryAWSClients.SentryS3()
s3_trigger_bucket = "sentry-test-emails-images-etc"

file_to_read = "C:/Users/sjoshi/Downloads/Email_Arlo_ursvu15to92blleok1dg2i8eeqbsvf5ov6bg8eg1"

def test_EmailProcessor ():
    logger.info("Just testing test_EmailProcessor()................")
    if file_to_read is None :
        return 'Cannot read anything as file_to_read is null'

    print (file_to_read)
    keys = file_to_read.split("/")
    key = keys[len(keys)-1]
    print("key is " + key)

    # Check if this file's already in S3
    testObj = s3_client.get_from_s3(s3_trigger_bucket, key)

    if testObj is None:
        with open(file_to_read,"r") as f:
            data = f.read()
            s3_client.put_into_s3(s3_trigger_bucket, key, data)

    # now that we have the data pushed to s3
    msg = ep.load_email(key, s3_trigger_bucket)
    email_processor = ep.get_right_processor(msg)

    bits = file_to_read.split("_")
    processor = str(email_processor.__class__.__name__)
    print( processor )
    print(bits[1])
    return  (processor.startswith(bits[1]) != False)

def test_EmailProcessors_From_S3Bucket_files ():
    logger.error("Just testing test_EmailProcessors_From_S3Bucket_files...............")
    results = {}
    # Check if this file's already in S3
    for key in s3_client.get_matching_s3_keys( s3_trigger_bucket, 'Email'):
        logger.error("Processing key ------------->" + str(key))
        # now that we have the key from s3 bucket
        msg = ep.load_email(key, s3_trigger_bucket, False)
        email_processor = ep.get_right_processor(msg, False)

        bits = key.split("_")
        processor = str(email_processor.__class__.__name__)
        #print( processor )
        #print(bits[1])
        val = (processor.startswith(bits[1]) != False)
        results[processor] = val
        logger.error("Results are ------------->" + str(results))
    return results