import os
os.environ['SQS_Queue'] = 'dev-people-api-incoming.fifo'

import lambda_function
import logging


logs_format = '%(asctime)s %(name)-20s %(levelname)-8s %(message)s'
logging.basicConfig(level='INFO',
                    format=logs_format)

event = {
  "Records": [
    {
      "eventVersion": "2.0",
      "eventSource": "aws:s3",
      "awsRegion": "us-east-1",
      "event": "1970-01-01T00:00:00.000Z",
      "eventName": "event-type",
      "userIdentity": {
        "principalId": "Amazon-customer-ID-of-the-user-who-caused-the-event"
      },
      "requestParameters": {
        "sourceIPAddress": "ip-address-where-request-came-from"
      },
      "responseElements": {
        "x-amz-request-id": "Amazon S3 get nerated request ID",
        "x-amz-id-2": "Amazon S3 host that processed the request"
      },
      "s3": {
        "s3SchemaVersion": "1.0",
        "configurationId": "ID found in the bucket notification configuration",
        "bucket": {
          "name": "sentry-test-emails-images-etc",
          "ownerIdentity": {
            "principalId": "Amazon-customer-ID-of-the-bucket-owner"
          },
          "arn": "bucket-ARN"
        },
        "object": {
          # "key":"ga63g4320vmmtcpr9437h2albete5911c7152ro1", #Foscam
          # "key":"20248158624463020180629171054412109",   # Causes error in people result handler
          # "key":"g1024ek4kscs527mv14nj21eponnfd50ebgn4j01",   # 936 Test with camera name
          # "key":"4i6luncc7rpgteunotk0rscumpbgvj5sivhn0jg1", # 2330 Test with camera name
          # "key":"q5dh8ptp2t4n4okepcurbevmjlu28ohp6o0p5b01", # Nestcam
          # "key": "trhovo868lpp05pokplprmb3ilq2ovqpnqbg8eg1",  #Amcrest
          "key": "Email_Arlo_Pro_raj3471kd53b66fdllfa7hs8cm2dp9bn92pi07g1.txt",  #ArloPro
          # "key": "0ok98h6r4cips4bk7490bmrhlh6hbfgoi0887f81",
          # "key": "test2330_trishul",
          "size": "object-size",
          "eTag": "object eTag",
          "versionId": "object version if bucket is versioning-enabled, otherwise null",
          "sequencer": "a string representation of a hexad"
        }
      }
    }
  ]
}

lambda_function.lambda_handler(event, None)
